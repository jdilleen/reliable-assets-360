import Link from "next/link";
import React, { useState } from "react";

export default function Header() {
    const [isOpen, setIsOpen] = useState(false);
    const buttonHandler = () => {
        setIsOpen((current) => !current);
    };

    const pages = ["asset monitor", "contact"];

    return (
        <nav className="w-full sm:w-4/5 mx-auto px-3 sm:px-0">
            <div className="relative flex items-center justify-between h-16">
                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                    <button
                        className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                        aria-expanded="false"
                        onClick={buttonHandler}
                    >
                        <span className="sr-only">Open main menu</span>
                        <svg
                            className="block h-6 w-6"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                        >
                            {isOpen ? (
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth="2"
                                    d="M6 18L18 6M6 6l12 12"
                                />
                            ) : (
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth="2"
                                    d="M4 6h16M4 12h16M4 18h16"
                                />
                            )}
                        </svg>
                    </button>
                </div>
                <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                    <div className="flex-shrink-0 flex items-center">
                        <Link href="/">
                            <a>
                                <img
                                    src="/orig_icon.png"
                                    alt="Reliable Assets 360"
                                    className="block lg:hidden h-8 w-auto"
                                />
                                <img
                                    src="/Original on Transparent.png"
                                    alt="Reliable Assets 360"
                                    className="hidden lg:block h-12 w-auto"
                                />
                            </a>
                        </Link>
                    </div>
                    <div className="hidden sm:block sm:ml-auto my-auto">
                        <div className="flex space-x-4">
                            {pages.map((page, index) => (
                                <Link href={`/${page.replace(" ", "-")}`} key={index}>
                                    <a className="px-3 py-2 rounded-md text-sm font-medium text-purple-700 hover:text-white hover:bg-gray-800 capitalize">
                                        {page}
                                    </a>
                                </Link>
                            ))}
                        </div>
                    </div>
                </div>
            </div>

            {isOpen && (
                <div className="px-2 pt-2 pb-3 space-y-1">
                    {pages.map((page, index) => (
                        <Link href={`/${page.replace(" ", "-")}`} key={index}>
                            <a className="block py-2 text-center rounded-md text-base font-medium text-purple-700 hover:text-white hover:bg-gray-700 capitalize">
                                {page}
                            </a>
                        </Link>
                    ))}
                </div>
            )}
        </nav>
    );
}
