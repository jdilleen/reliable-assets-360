import Head from "next/head";
import Link from "next/link";
import Contact from "../pages/contact";
import Footer from "./Footer";
import Header from "./Header";

const SiteLayout = ({ children }) => (
    <div className="bg-gray-900 text-gray-100 antialiased flex flex-col min-h-screen">
        <div className="bg-white border-b-4 border-orange-500">
            <Header />
        </div>
        <div className="flex-grow w-full sm:w-4/5 mx-auto px-3 sm:px-0">
            <div className="mt-6">{children}</div>
        </div>
        <Footer />
    </div>
);

export default SiteLayout;
