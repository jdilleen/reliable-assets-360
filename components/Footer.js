import Link from "next/link";
import React, { useState } from "react";

export default function Footer() {

    return (
        <footer className="flex w-full sm:w-4/5 mx-auto px-3 sm:px-0 py-4 text-sm">
            <div>&copy; {new Date().getFullYear()} | Reliable Assets 360</div>
            <div className="ml-auto">
                <Link href="/contact">
                    <a className="hover:text-purple-300">Contact</a>
                </Link>
            </div>
        </footer>
    );
}
