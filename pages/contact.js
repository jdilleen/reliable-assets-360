import Head from "next/head";
import SiteLayout from "../components/SiteLayout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCoffee,
    faEnvelope,
    faMapMarker,
    faPhone,
} from "@fortawesome/free-solid-svg-icons";

export default function Contact() {
    return (
        <div>
            <Head>
                <title>Contact | Reliable Assets 360</title>
            </Head>
            <SiteLayout>
                <div className="sm:w-2/3 mx-auto">
                    <div className="pt-10 text-center">
                        <div className="text-3xl sm:text-4xl text-purple-400 font-extrabold mb-4">
                            Ready to hear more?
                        </div>
                        <div className="sm:text-lg text-gray-300 mb-4 sm:mb-6">
                            Reach out to us for your reliability needs. We will
                            be glad to visit your facility, take a look and
                            partner with you to help reduce your maintenance
                            costs and assist you in improving the overall plant
                            reliability and efficiency.
                        </div>
                    </div>
                    <div className="grid grid-rows-4 sm:grid-rows-2 grid-cols-1 sm:grid-cols-3 gap-2 sm:gap-x-4 sm:gap-y-6 py-10 sm:py-20">
                        <div className="text-3xl font-extrabold">
                            Get in touch
                        </div>
                        <div>
                            <div className="text-lg font-bold mb-1">
                                <FontAwesomeIcon
                                    icon={faPhone}
                                    fixedWidth
                                    className="mr-3 text-purple-400"
                                />
                                Phone
                            </div>
                            <div>
                                <a href="tel:+1 (832) 689 6047">
                                    +1 (832) 689 6047
                                </a>
                            </div>
                        </div>
                        <div>
                            <div className="text-lg font-bold mb-1">
                                <FontAwesomeIcon
                                    icon={faEnvelope}
                                    fixedWidth
                                    className="mr-3 text-purple-400"
                                />
                                Email
                            </div>
                            <div>
                                <a href="mailto:admin@reliableassets360.com">
                                    admin@reliableassets360.com
                                </a>
                            </div>
                        </div>
                        <div className="hidden sm:block"></div>
                        <div>
                            <div className="text-lg font-bold mb-1">
                                <FontAwesomeIcon
                                    icon={faMapMarker}
                                    fixedWidth
                                    className="mr-3 text-purple-400"
                                />
                                Address
                            </div>
                            <div>
                                17511 Warm Winds Drive,
                                <br />
                                Tomball,
                                <br />
                                TX 77377
                            </div>
                        </div>
                        <div className="hidden sm:block"></div>
                    </div>
                </div>
            </SiteLayout>
        </div>
    );
}
