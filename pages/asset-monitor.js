import { faTachometerAlt, faWater } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Link from "next/link";
import SiteLayout from "../components/SiteLayout";

export default function Home() {
    return (
        <div>
            <Head>
                <title>Asset Monitor | Reliable Assets 360</title>
            </Head>
            <SiteLayout>
                <div className="text-center py-10">
                    <div className="font-extrabold text-4xl sm:text-5xl mb-4">
                        <span className="bg-clip-text text-transparent bg-gradient-to-tr from-orange-400 to-purple-600">
                            Asset Monitor
                        </span>
                    </div>
                    <div className="sm:text-lg">
                        Product features include Continuous monitoring of
                        Pressure, Temperature and Vibration on an asset. The
                        hardware includes the relevant sensors, cables and the
                        control unit to process the data and transfer the data
                        to a central repository. The data is then contextualized
                        and stored for easy retrieval via an app over the iOS or
                        android. The app is customizable and has the ability to
                        plot pressure, temperature and vibration of any number
                        of assets. It also has the ability to create
                        user-defined alarms on upper and lower thresholds.
                    </div>
                </div>

                <div className="grid grow-rows-2 sm:grid-rows-1 grid-cols-1 sm:grid-cols-2 sm:gap-4 mb-6">
                    <div className="mb-10">
                        <div className="text-lg sm:text-2xl font-extrabold mb-4">
                            Benefits
                        </div>
                        <div className="mb-10">
                            The ability to detect an alarm allows the user to
                            probe into the diagnosis of the asset and prevent
                            any potential catastrophic failure. It allows the
                            user to improve the uptime of the asset, thereby
                            increasing the efficiency and productivity of their
                            processes. The ability to increase runtime of the
                            assets, allows the users to increase the reliability
                            of their assets, systems and processes all the time.
                            Users can monitor the real time health of assets
                            remotely from anywhere and at any time using the
                            app.
                        </div>

                        <div className="text-lg sm:text-2xl font-extrabold mb-4">
                            Technical Features
                        </div>
                        <div>
                            <dl className="space-y-10">
                                <div className="flex">
                                    <div className="flex-shrink-0">
                                        <div className="flex items-center justify-center h-12 w-12 rounded-md bg-purple-500 text-white">
                                            <FontAwesomeIcon
                                                fixedWidth
                                                icon={faTachometerAlt}
                                                size="lg"
                                            />
                                        </div>
                                    </div>
                                    <div className="ml-4">
                                        <dt className="text-lg font-semibold leading-6 text-gray-200">
                                            Pressure Monitoring
                                        </dt>
                                        <dd className="mt-1 text-base">
                                            Pressure range: 0 to 5000 psi
                                            <br />
                                            Pressure type: Gage or absolute
                                            <br />
                                            Operating temperature: -40&#176;C to
                                            125&#176;C (-40&#176;F to
                                            250&#176;F)
                                        </dd>
                                    </div>
                                </div>

                                <div className="flex">
                                    <div className="flex-shrink-0">
                                        <div className="flex items-center justify-center h-12 w-12 rounded-md bg-purple-500 text-white">
                                            <FontAwesomeIcon
                                                fixedWidth
                                                icon={faWater}
                                                size="lg"
                                            />
                                        </div>
                                    </div>
                                    <div className="ml-4">
                                        <dt className="text-lg font-semibold leading-6 text-gray-200">
                                            Vibration Monitoring
                                        </dt>
                                        <dd className="mt-1 text-base">
                                            Dynamic range: ±80 G
                                            <br />
                                            Frequency range: 2 Hz to 10 kHz ±5 %
                                            <br />
                                            Operating temperature : 12&#176;C to
                                            138&#176;C (-10&#176;F to
                                            280&#176;F)
                                        </dd>
                                    </div>
                                </div>

                                <div className="flex">
                                    <div className="flex-shrink-0">
                                        <div className="flex items-center justify-center h-12 w-12 rounded-md bg-purple-500 text-white">
                                            <FontAwesomeIcon
                                                fixedWidth
                                                icon={faWater}
                                                size="lg"
                                            />
                                        </div>
                                    </div>
                                    <div className="ml-4">
                                        <dt className="text-lg font-semibold leading-6 text-gray-200">
                                            Temperature Monitoring
                                        </dt>
                                        <dd className="mt-1 text-base">
                                            Temperature range : -18&#176;C to
                                            93&#176;C (0&#176;F to 200&#176;F)
                                        </dd>
                                    </div>
                                </div>
                            </dl>
                        </div>
                    </div>
                    <div className="mt-10 sm:mt-28">
                        <div className="relative w-2/3 mx-auto py-3">
                            <div className="absolute inset-0 bg-gradient-to-br from-orange-400 to-purple-500 transform rotate-6 rounded-3xl"></div>
                            <div className="relative bg-white shadow-lg rounded-3xl p-10">
                                <img
                                    src="/app_list_screenshot.png"
                                    alt="App Charts"
                                    className="shadow-lg mx-auto bg-gray-100 rounded-3xl h-72 sm:h-96"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="py-10 text-center">
                    <div className="text-3xl sm:text-4xl text-purple-400 font-extrabold mb-4">
                        Ready to hear more?
                    </div>
                    <div className="sm:text-lg text-gray-300 mb-6">
                        Reach out to us for your reliability needs. We will be
                        glad to visit your facility, take a look and partner
                        with you to help reduce your maintenance costs and
                        assist you in improving the overall plant reliability
                        and efficiency.
                    </div>
                    <div className="mx-auto">
                        <div className="inline-flex mr-2">
                            <Link href="/contact">
                                <a className="py-3 px-4 bg-purple-500 hover:bg-purple-600 text-white font-bold rounded-lg shadow-lg">
                                    Contact Us
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </SiteLayout>
        </div>
    );
}
