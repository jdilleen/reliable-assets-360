import Head from "next/head";
import Link from "next/link";
import SiteLayout from "../components/SiteLayout";

export default function Home() {
    return (
        <div>
            <Head>
                <title>Home | Reliable Assets 360</title>
            </Head>
            <SiteLayout>
                <div className="grid grow-rows-2 sm:grid-rows-1 grid-cols-1 sm:grid-cols-2 sm:gap-4 mb-6">
                    <div className="mb-10 sm:my-auto">
                        <div className="font-extrabold text-4xl sm:text-5xl mb-4">
                            <span className="bg-clip-text text-transparent bg-gradient-to-tr from-orange-400 to-purple-600">
                                Reliable Assets 360
                            </span>
                        </div>
                        <div className="sm:text-lg">
                            Reliable Assets 360 provides a holistic 360° view of
                            your industrial assets in real-time and from
                            anywhere. We provide a set of robust hardware that
                            works with a secure and safe software suite to
                            continuously monitor, alert and alarm you when there
                            are any abnormal machinery events. We also offer
                            services to enable on-site assistance to ensure that
                            our customers can meet their machinery uptime,
                            productivity, reliability and efficiency needs
                            following the highest safety standards.
                        </div>
                    </div>
                    <div className="py-10">
                        <div className="relative w-2/3 mx-auto py-3">
                            <div className="absolute inset-0 bg-gradient-to-br from-orange-400 to-purple-500 transform rotate-6 rounded-3xl"></div>
                            <div className="relative bg-white shadow-lg rounded-3xl p-10">
                                <img
                                    src="/chart_in_phone.png"
                                    alt="App Charts"
                                    className="shadow-lg mx-auto bg-gray-100 rounded-3xl h-72 sm:h-96"
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="sm:w-4/5 mx-auto py-10">
                    <div className="text-center sm:text-lg uppercase text-purple-400 font-bold mb-1">
                        From reactive, time based monitoring to...
                    </div>
                    <div className="text-center text-2xl sm:text-4xl font-extrabold mb-4">
                        Proactive Condition Based Monitoring
                    </div>
                    <div className="mb-16">
                        Manufacturing facilities are always challenged to
                        deliver maximum profitability. One way to deliver
                        profitability is to minimize non-value add costs. For a
                        long time, maintenance practices have been tied to time
                        based maintenance practices, simply because that’s the
                        way it has been for a long time. Moving from a time
                        based to a proactive condition based maintenance
                        strategy may save costs by increasing maintenance
                        intervals on machines that don’t necessarily need
                        maintenance. A proactive condition based maintenance
                        solution will allow more focus on the asset that needs
                        attention and helps prioritizing work orders
                        accordingly.
                    </div>
                    <div className="text-lg sm:text-2xl font-extrabold mb-4">
                        Servicing the equipment before it causes a major plant
                        upset
                    </div>
                    <div className="mb-16">
                        More often than not, major catastrophes can be avoided
                        if minor defects and early symptoms are caught early in
                        the process. There is almost always an early indicator
                        of a failure that manifests itself in a temperature,
                        vibration or a pressure change, depending on the
                        application. Reliable Assets 360 provide a complete 360°
                        approach to detect these early faults and provide the
                        window to fix a minor fault before it could cause a
                        major shutdown.
                    </div>
                    <div className="text-lg sm:text-2xl font-extrabold mb-4">
                        The COVID pandemic has expedited the Digital
                        transformation journey
                    </div>
                    <div className="mb-12">
                        Safety of our employees remains our top priority, now
                        more than ever as we live through the global COVID
                        pandemic. Until then, plants still need to operate and
                        machines still need to run. Maintenance teams are
                        challenged to ensure high uptime of the machines by
                        having to physically inspect the machines. Physically
                        inspecting the machine would mean travelling to the
                        physical site and staying there for extended periods of
                        time exposing human personnel to safety risks. Reliable
                        Assets 360 allows for sensors to do the data collection
                        and let’s humans do the analytics remotely, thereby
                        minimizing their risk to the covid exposures.
                        Maintenance personnel can still view the data, monitor
                        the health of their machines, watch for alerts and
                        alarms remotely and be just as effective.
                    </div>
                    <div className="py-10 text-center">
                        <div className="text-3xl sm:text-4xl text-purple-400 font-extrabold mb-4">
                            Ready to hear more?
                        </div>
                        <div className="sm:text-lg text-gray-300 mb-6">
                            Reach out to us for your reliability needs. We will
                            be glad to visit your facility, take a look and
                            partner with you to help reduce your maintenance
                            costs and assist you in improving the overall plant
                            reliability and efficiency.
                        </div>
                        <div className="mx-auto">
                            <div className="inline-flex mr-2">
                                <Link href="/contact">
                                    <a className="py-3 px-4 bg-purple-500 hover:bg-purple-600 text-white font-bold rounded-lg shadow-lg">
                                        Contact Us
                                    </a>
                                </Link>
                            </div>
                            <div className="inline-flex ml-2">
                                <Link href="/asset-monitor">
                                    <a className="py-3 px-4 bg-white hover:bg-purple-100 text-purple-500 hover:text-purple-700 font-bold rounded-lg shadow-lg">
                                        Learn More
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </SiteLayout>
        </div>
    );
}
